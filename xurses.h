#include <xolatile/xtandard.h>

#include <termios.h>
#include <sys/ioctl.h>

#define curses_format_length ((int) sizeof ("\033[-;3-m-\033[0m") - 1)
#define curses_revert_length ((int) sizeof ("\033[H")             - 1)
#define curses_cursor_length ((int) sizeof ("\033[---;---H")      - 1)

static char curses_format [curses_format_length + 1] = "\033[-;3-m-\033[0m";
static char curses_cursor [curses_cursor_length + 1] = "\033[---;---H";

static char * curses_screen = null;

static void (* curses_action [signal_count]) (void) = { null };

static struct termios curses_old_terminal;
static struct termios curses_new_terminal;

static int curses_screen_width  = 0;
static int curses_screen_height = 0;
static int curses_active        = false;

static void curses_memory (void) {
	curses_screen = deallocate (curses_screen);

	terminal_clear ();

	fatal_failure (tcsetattr (STDIN_FILENO, TCSAFLUSH, & curses_old_terminal) == -1, "tcsetattr: Failed to set default attributes.");
}

static char * curses_screen_offset (int x, int y) {
	limit (& x, 0, curses_screen_width  - 1);
	limit (& y, 0, curses_screen_height - 1);

	return (& curses_screen [curses_revert_length + curses_format_length * (y * curses_screen_width + x) + 2 * y]);
}

static int curses_screen_length (void) {
	int constant, variable, new_line;

	constant = curses_revert_length + curses_cursor_length + 1;
	variable = curses_format_length * curses_screen_height * curses_screen_width;
	new_line = curses_screen_height - 1;

	return (constant + variable + 2 * new_line);
}

static void curses_screen_dimensions (void) {
	int index, old_width, old_height;

	struct winsize screen_dimension;

	old_width  = curses_screen_width;
	old_height = curses_screen_height;

	fatal_failure (ioctl (STDOUT_FILENO, TIOCGWINSZ, & screen_dimension) == -1, "ioctl: Failed to get dimensions.");

	curses_screen_width  = (int) screen_dimension.ws_col;
	curses_screen_height = (int) screen_dimension.ws_row;

	if ((old_width != curses_screen_width) || (old_height != curses_screen_height)) {
		curses_screen = deallocate (curses_screen);
		curses_screen = allocate   (curses_screen_length ());
	}

	string_copy (& curses_screen [0], "\033[H");

	for (index = 0; index < curses_screen_height - 1; ++index) {
		string_copy (& curses_screen [curses_revert_length + index * curses_format_length * curses_screen_width], "\r\n");
	}
}

static char * curses_format_character (char character, int colour, int effect) {
	if (character_is_invisible (character) != 0) {
		character = ' ';
	}

	colour %= colour_count;
	effect %= effect_count;

	switch (effect) {
		case effect_normal:    effect = 0; break;
		case effect_bold:      effect = 1; break;
		case effect_italic:    effect = 3; break;
		case effect_underline: effect = 4; break;
		case effect_blink:     effect = 5; break;
		case effect_reverse:   effect = 7; break;
		default:               effect = 0; break;
	}

	curses_format [2] = (char) effect + '0';
	curses_format [5] = (char) colour + '0';
	curses_format [7] = character;

	return (curses_format);
}

static void curses_idle (void) {
	return;
}

static void curses_exit (void) {
	curses_active = false;
}

static void curses_bind (int signal, void (* action) (void)) {
	curses_action [signal % signal_count] = action;
}

static void curses_unbind (int signal) {
	curses_action [signal % signal_count] = curses_idle;
}

static void curses_configure (void) {
	int index;

	if (curses_active == true) {
		return;
	} else {
		clean_up (curses_memory);
	}

	curses_screen_dimensions ();

	fatal_failure (tcgetattr (STDIN_FILENO, & curses_old_terminal) == -1, "tcgetattr: Failed to get default attributes.");

	curses_new_terminal = curses_old_terminal;

	curses_new_terminal.c_cc [VMIN]  = (unsigned char) 0;
	curses_new_terminal.c_cc [VTIME] = (unsigned char) 1;

	curses_new_terminal.c_iflag &= (unsigned int) ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	curses_new_terminal.c_oflag &= (unsigned int) ~(OPOST);
	curses_new_terminal.c_cflag |= (unsigned int)  (CS8);
	curses_new_terminal.c_lflag &= (unsigned int) ~(ECHO | ICANON | IEXTEN | ISIG);

	fatal_failure (tcsetattr (STDIN_FILENO, TCSAFLUSH, & curses_new_terminal) == -1, "tcsetattr: Failed to set reverse attributes.");

	for (index = 0; index < signal_count; ++index) {
		curses_bind (index, curses_idle);
	}

	curses_bind (signal_escape, curses_exit);

	terminal_clear ();

	curses_active = true;
}

static void curses_synchronize (void) {
	char character = '\0';
	int  signal    = signal_none;

	input (& character, (int) sizeof (character));

	output (curses_screen, curses_screen_length ());

	curses_screen_dimensions ();

	if (character == '\033') {
		signal = signal_escape;
	} else if (character_is_digit (character) == true) {
		signal = signal_0 + (int) character - '0';
	} else if (character_is_lowercase (character) == true) {
		signal = signal_a + (int) character - 'a';
	} else if (character_is_uppercase (character) == true) {
		signal = signal_a + (int) character - 'A';
	} else {
		signal = signal_none;
	}

	curses_action [signal % signal_count] ();
}

static void curses_render_cursor (int x, int y) { /* BROKE IT INTENTIONALLY */
	string_copy_limit (curses_cursor + 2, string_realign (number_to_string (y % 1000 + 1), 3, '0'), 3);
	string_copy_limit (curses_cursor + 6, string_realign (number_to_string (x % 1000 + 1), 3, '0'), 3);

	string_copy_limit (& curses_screen [curses_screen_length () - curses_cursor_length - 1], curses_cursor, curses_cursor_length);
}

static void curses_render_character (char character, int colour, int effect, int x, int y) {
	limit (& x, 0, curses_screen_width  - 1);
	limit (& y, 0, curses_screen_height - 1);

	string_copy_limit (curses_screen_offset (x, y), curses_format_character (character, colour, effect), curses_format_length);
}

static void curses_render_string (char * string, int colour, int effect, int x, int y) {
	int index;

	for (index = 0; string [index] != '\0'; ++index) {
		curses_render_character (string [index], colour, effect, x + index, y);
	}
}

static void curses_render_number (int number, int colour, int effect, int x, int y) {
	curses_render_string (number_to_string (number), colour, effect, x, y);
}

static void curses_render_string_crop (char * string, int colour, int effect, int x, int y, int crop) {
	int index;

	for (index = 0; (string [index] != '\0') && (index < crop); ++index) {
		curses_render_character (string [index], colour, effect, x + index, y);
	}
}

static void curses_render_number_crop (int number, int colour, int effect, int x, int y, int crop) {
	curses_render_string_crop (number_to_string (number), colour, effect, x, y, crop);
}

static void curses_render_vertical_line (char character, int colour, int effect, int x, int y, int height) {
	int offset;

	for (offset = 0; offset != height; ++offset) {
		curses_render_character (character, colour, effect, x, y + offset);
	}
}

static void curses_render_horizontal_line (char character, int colour, int effect, int x, int y, int width) {
	int offset;

	for (offset = 0; offset != width; ++offset) {
		curses_render_character (character, colour, effect, x + offset, y);
	}
}

static void curses_render_rectangle_line (char character, int colour, int effect, int x, int y, int width, int height) {
	curses_render_vertical_line   (character, colour, effect, x + 0,         y + 0,          height + 0);
	curses_render_vertical_line   (character, colour, effect, x + width - 1, y + 0,          height + 0);
	curses_render_horizontal_line (character, colour, effect, x + 1,         y + 0,          width  - 1);
	curses_render_horizontal_line (character, colour, effect, x + 1,         y + height - 1, width  - 1);
}

static void curses_render_rectangle_fill (char character, int colour, int effect, int x, int y, int width, int height) {
	int offset_x, offset_y;

	for (offset_y = 0; offset_y != height; ++offset_y) {
		for (offset_x = 0; offset_x != width; ++offset_x) {
			curses_render_character (character, colour, effect, x + offset_x, y + offset_y);
		}
	}
}


static void curses_render_background (char character, int colour, int effect) {
	int x, y;

	for (y = 0; y != curses_screen_height; ++y) {
		for (x = 0; x != curses_screen_width; ++x) {
			curses_render_character (character, colour, effect, x, y);
		}
	}
}
