#!/bin/bash

set -xe

mkdir -p /usr/local/include/xolatile

cp xurses.h /usr/local/include/xolatile/xurses.h

exit
