# xurses

## xurses -- Library for creation of simple programs in terminal.

Install: `$ sudo sh install.sh`

Include: `#include <xolatile/xurses.h>`

This library depends on:
- xtandard: https://gitlab.com/xolatile/xtandard

About this program:
- I don't know what else to write here, program is being done in my free time, so expect more...
- Everything related to my libraries is clean of all warning options on Clang, GCC and Valgrind.
