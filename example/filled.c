#include <xolatile/xurses.h>

int main (void) {
	curses_configure ();

	while (curses_active == true) {
		curses_render_background ('.', colour_grey, effect_bold);

		curses_render_horizontal_line ('-', colour_red, effect_bold, 0, 0, 10);

		curses_render_rectangle_line ('#', colour_cyan, effect_normal,  2, 2, 5, 5);
		curses_render_rectangle_fill ('H', colour_blue, effect_normal, 12, 2, 5, 5);

		curses_synchronize ();
	}

	return (log_success);
}
