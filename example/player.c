#include <xolatile/xurses.h>

static int player_x   = 1;
static int player_y   = 1;
static int map_width  = 80;
static int map_height = 24;

static void move_up    (void) { --player_y; limit (& player_y, 1, map_height - 1); }
static void move_down  (void) { ++player_y; limit (& player_y, 1, map_height - 1); }
static void move_left  (void) { --player_x; limit (& player_x, 1, map_width  - 1); }
static void move_right (void) { ++player_x; limit (& player_x, 1, map_width  - 1); }

int main (void) {
	curses_configure ();

	curses_bind (signal_q, curses_exit);
	curses_bind (signal_w, move_up);
	curses_bind (signal_s, move_down);
	curses_bind (signal_a, move_left);
	curses_bind (signal_d, move_right);

	while (curses_active == true) {
		curses_render_background ('.', colour_grey, effect_bold);

		curses_render_rectangle_line ('#', colour_blue, effect_reverse, 0, 0, map_width + 1, map_height + 1);
		curses_render_rectangle_fill ('.', colour_cyan, effect_normal,  1, 1, map_width - 1, map_height - 1);

		curses_render_character ('@', colour_green, effect_bold, player_x, player_y);

		curses_synchronize ();
	}

	return (log_success);
}
