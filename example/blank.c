#include <xolatile/xurses.h>

int main (void) {
	curses_configure ();

	while (curses_active == true) {
		curses_synchronize ();
	}

	return (log_success);
}
